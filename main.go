package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly/v2"
)

type Service struct {
	Name string
	URL  string
}

type Network struct {
	Name     string
	Services []Service
}

type Group struct {
	Name     string
	Networks []Network
}

var groups []Group

type ScheduledProgramme struct {
	Service     *Service
	Time        time.Time
	Name        string
	Description string
	URL         string
}

const baseURL = "https://www.bbc.co.uk"
const domain = "www.bbc.co.uk"

func main() {
	services := scrapeBBC()
	scheduledProgrammes := scrapeServices(services)
	_ = scheduledProgrammes // TODO do something with them
}

func scrapeBBC() []Service {
	c := colly.NewCollector(
		colly.AllowedDomains(domain),
	)

	// A group groups TV, Radio, or similar programmes together
	c.OnHTML("div.sch-group", scrapeBBC_Group)

	c.Visit(baseURL + "/schedules")

	var services []Service
	for _, group := range groups {
		for _, network := range group.Networks {
			for _, service := range network.Services {
				services = append(services, service)
			}
		}
	}

	return services
}

func scrapeBBC_Group(e *colly.HTMLElement) {
	group := Group{
		Name: e.ChildText("h2.alpha"),
	}
	//    fmt.Println("Group: " + group.Name)
	groups = append(groups, group)

	e.ForEach("div.sch-network-name", scrapeBBC_Network)
}

func scrapeBBC_Network(i int, e *colly.HTMLElement) {
	network := Network{
		Name: e.Text,
	}
	//    fmt.Println("\tNetwork: " + network.Name)
	group := &groups[len(groups)-1]
	(*group).Networks = append((*group).Networks, network)

	// Unfortunately Networks & Services are mixed in this list, that's why
	// we need to find all Networks and then jump along to the siblings from
	// there til we reach the next Network.
	//
	// Worse, though, if a Network contains just one Service, it isn't listed
	// as a Network in the main list, but as a Service, with a Network child:
	//
	// * Network A
	//    - Service A:A
	//    - Service B:A
	// * Network B
	//    - Service B:A
	//    - Service B:B
	// * Service C:_
	//    - Network C
	//
	// I have no idea who came up with this "structure"...

	if e.DOM.Parent().Is("a.sch-service-row") {
		/* the Network we found is a child of the only Service it provides */
		scrapeBBC_Service(0, e.DOM.Parent())
	} else {
		e.DOM.NextUntil("div.sch-network-name").Each(scrapeBBC_Service)
	}
}

func scrapeBBC_Service(i int, s *goquery.Selection) {
	if i != 0 && s.Children().First().Is("div.sch-network-name") {
		// We are looking for another Service to add, but we've found
		// a collapsed single-Service Network instead.
		return
	}
	url, exists := s.Attr("href")
	if !exists {
		panic("service doesn't contain link")
	}
	name := s.ChildrenFiltered("div.sch-service-name").Text()
	if name == "" {
		// If we are a Network providing a single Service, use the
		// Network's Name as the Service
		name = s.ChildrenFiltered("div.sch-network-name").Text()
	}
	//    fmt.Println("\t\tService: " + name + " at: " + url)

	service := Service{
		Name: name,
		URL:  url,
	}
	networks := groups[len(groups)-1].Networks
	network := &networks[len(networks)-1]
	(*network).Services = append((*network).Services, service)
}

func scrapeServices(services []Service) []ScheduledProgramme {
	var scheduledProgrammes []ScheduledProgramme
	for _, service := range services {
		scheduledProgrammes = append(scheduledProgrammes, scrapeService(service)...)
	}

	return scheduledProgrammes
}

func scrapeService(service Service) []ScheduledProgramme {
	c := colly.NewCollector(
		colly.AllowedDomains(domain),
		colly.MaxDepth(1),
	)

	year, month, day := time.Now().Date()
	pc := parseContext{&service, year, month, day}

	var scheduledProgrammes []ScheduledProgramme
	c.OnHTML("div.broadcast", func(e *colly.HTMLElement) {
		scheduledProgramme, err := parseScheduledProgramme(e, pc)
		if err != nil {
			log.Printf("Failed to parse scheduled programme on %v: %v\n", baseURL+service.URL, err)
			return
		}

		scheduledProgrammes = append(scheduledProgrammes, scheduledProgramme)
	})

	url := fmt.Sprintf("%s%s/%d/%02d/%02d", baseURL, service.URL, year, month, day)
	log.Println("visiting", url)
	c.Visit(url)

	return scheduledProgrammes
}

type parseContext struct {
	service *Service
	year    int
	month   time.Month
	day     int
}

func parseScheduledProgramme(e *colly.HTMLElement, context parseContext) (ScheduledProgramme, error) {
	name := e.ChildText(".programme__title")

	description := e.ChildText(".programme__synopsis")

	var url string
	e.ForEachWithBreak("a.br-blocklink__link", func(_ int, elem *colly.HTMLElement) bool {
		url = elem.Attr("href")
		return false // break on first match
	})
	if url == "" {
		log.Printf("Could not find url for programme %q on %v\n", name, baseURL+context.service.URL)
	}

	location, err := getLocation()
	if err != nil {
		return ScheduledProgramme{}, err
	}

	const timeForm = "15:04"
	timeText := e.ChildText(".broadcast__time")
	if strings.HasSuffix(timeText, "GMT") {
		timeText = timeText[:len(timeText)-3]
		location, err = time.LoadLocation("GMT")
		if err != nil {
			return ScheduledProgramme{}, err
		}
	}
	t, err := time.Parse(timeForm, timeText)
	if err != nil {
		return ScheduledProgramme{}, err
	}

	sec, nsec := 0, 0

	programmeTime := time.Date(context.year, context.month, context.day, t.Hour(), t.Minute(), sec, nsec, location)

	return ScheduledProgramme{
		Service:     context.service,
		Time:        programmeTime,
		Name:        name,
		Description: description,
		URL:         url,
	}, nil
}

// Location cache, access with getLocation
var _location *time.Location

// getLocation loads the Location data from the time zone database or returns the cached value
func getLocation() (*time.Location, error) {
	if _location != nil {
		return _location, nil
	}

	london, err := time.LoadLocation("Europe/London")
	if err != nil {
		return nil, fmt.Errorf("Could not load location data for Europe/London")
	}

	_location = london
	return _location, nil
}
