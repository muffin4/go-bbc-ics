module ljrk.org/bbc-ics

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/antchfx/xmlquery v1.3.6 // indirect
	github.com/gocolly/colly/v2 v2.1.0
	github.com/temoto/robotstxt v1.1.2 // indirect
	golang.org/x/net v0.0.0-20210521195947-fe42d452be8f // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
